/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pukajmar
 */
public class StringProperty extends Property{

    protected StringProperty(String propertyName, String value) {
        super(propertyName, value);
    }

    @Override
    public Object getValue() {
        return value;
    }
    
}
