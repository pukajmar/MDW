/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pukajmar
 */
public class PersonProperty extends Property{

    protected PersonProperty(String propertyName, String value) {
        super(propertyName, value);
    }

    @Override
    public Object getValue() {
        String[] name = value.split("\\s+");
        
        JSONObject innerData = new JSONObject();
        try {
            innerData.put("name", name[0]);
            innerData.put("surname", name[1]);
            
            return innerData;
        } catch (JSONException ex) {
            Logger.getLogger(PersonProperty.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}
