/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pukajmar
 */
public class MailParser {

    protected String mainText;
    protected static final String TOKEN = "===";
    protected List<Property> properties;
    
    public MailParser(Path path) {
        
        String wholeText = "";
        
        try {
            Iterator<String> lineIterator = Files.lines(path).iterator();
            
            while(lineIterator.hasNext())
            {
                wholeText += lineIterator.next() + "\r\n";
            }
            
        } catch (IOException ex) {
            Logger.getLogger(MailParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        mainText = textBetweenTokens(wholeText, TOKEN);
        properties = new ArrayList();
        
        String[] lines = mainText.split(System.getProperty("line.separator"));
        
        for(String line : lines){

            Property prop = parseProperty(line);
            properties.add(prop);

        }
    }
    
    protected String textBetweenTokens(String text, String token)
    {
        int lengthOfToken = token.length();
        String result = text.substring(text.indexOf(token) + lengthOfToken, text.indexOf(token, text.indexOf(token) + lengthOfToken));
        
        result = result.trim();
        
        return result;
    }
    
    protected Property parseProperty(String line)
    {
        String[] data = line.split(":");
        
        return Property.create(data[0], data[1].replace("\"", ""));
    }
    
    public List<Property> getProperties()
    {
        return properties;
    }
    
}