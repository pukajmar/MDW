/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author pukajmar
 */
public abstract class Property {
    
    protected String propertyName;
    protected String value;
    
    public static Property create(String propertyName, String value){
        
        if("Person".equals(propertyName)){
            return new PersonProperty(propertyName, value);
        }
        if("Trip id".equals(propertyName)){
            return new IdProperty(propertyName, value);
        }
        else{
            return new StringProperty(propertyName, value);
        }
        
    }

    public String getPropertyName() {
        return propertyName.toLowerCase();
    }

    public abstract Object getValue();
    
    protected Property(String propertyName, String value)
    {
        this.propertyName = propertyName.trim();
        this.value = value.trim();
    }
    
}
