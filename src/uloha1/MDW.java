/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

import java.nio.file.FileSystems;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author pukajmar
 */
public class MDW {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        MailParser parser = new MailParser(FileSystems.getDefault().getPath("src/Resources", "input.txt"));

        
        JSONObject json = new JSONObject();
        
        for(Property prop : parser.getProperties()){
            try {
                json.put(prop.getPropertyName(), prop.getValue());
            } catch (JSONException ex) {
                Logger.getLogger(MDW.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println(json.toString());
        
        
    }
   
}
