/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uloha1;

/**
 *
 * @author pukajmar
 */
public class IdProperty extends Property{

    public IdProperty(String propertyName, String value) {
        super(propertyName, value);
    }
    
    @Override
    public String getPropertyName()
    {
        return "id";
    }

    @Override
    public Object getValue() {
        return value;
    }
    
}
